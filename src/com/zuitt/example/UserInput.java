package com.zuitt.example;
import java.util.Scanner;

public class UserInput {
    // We instantiate the myObj form the Scanner class
    // Scanner is used for obtaining input from the terminal.
    // "System.in" allows us to takes input from the console.
    public static void main(String[] args){
        Scanner myObject = new Scanner(System.in);
     /*   System.out.println("Enter username: ");
        // nextLine()  capture input --> will take the input
        String userName = myObject.nextLine();
//        System.out.println("Username is : " + userName);
        System.out.println(userName + userName);*/

        System.out.println("Enter a number to add ");
        System.out.println("Enter first number: ");
//        int num1 = Integer.parseInt(myObject.nextLine());
//        shorthand method
        int num1 = myObject.nextInt();
        System.out.println("Enter second number: ");
        int num2 = myObject.nextInt();
        System.out.println("Sum of two numbers is: " + (num1 + num2));

    }
}
