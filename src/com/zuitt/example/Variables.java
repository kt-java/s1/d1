//A "package" in Java is used to group related classes. Think of it as a folder in a file directory.
// Packages are divided into two categories:
// 1.Built-in Packages (packages from the Java API)
// 2. User-defined Packages (create your own packages)

// Package creation follows the "reverse domain name notation" for the naming convention.
// The syntax is useful because the ordering of the named components gets reversed, surfacing the logical groupings inherent in the designed structure of DNS.
package com.zuitt.example;

public class Variables {
    public static void main(String[] args){
        // Naming convention

        // The terminology used for variable names is "identifier".
        // All identifiers should begin with a letter (A to Z or a to z), currency character ($) or an underscore.
        // After the first character, identifiers can have any combination of characters.
        // A keyword cannot be used as an identifier.
        // Most importantly, identifiers are case sensitive.
        // Syntax: dataType identifier

        // variable
        int age;
        char middleName;

        // var declaration vs initialization
        int x;
        int y = 0;

        // init after declaration
        x = 1;

        System.out.println("The value of y is " + y + " and the value of x is " + x);

        int wholeNumber = 100;
        System.out.println(wholeNumber);

        long worldPopulation = 65416516546845L;
        System.out.println(worldPopulation);

        float piFloat = 3.14159265359f;
        System.out.println(piFloat);

        double piDouble = 3.14159265359d;
        System.out.println(piDouble);

        char letter = 'a';
        System.out.println(letter);

        boolean isLove = true;
        boolean isTaken = false;

        System.out.println(isLove);
        System.out.println(isTaken);

        //const
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        //PRINCIPAL = 4000; --> error cant change const value

        // Non-primitive data
        // Also known as reference data types refer to instances or objects.
        // do not directly store the value of a variable, but rather remembers the reference to that variable.

        // String
        // stores a sequence or array of characters.
        // String are actually object that can use methods

        String username = "JSmith";
        System.out.println(username);

        //Sample string method
        int stringLength = username.length();
        System.out.println(stringLength);
    }
}
